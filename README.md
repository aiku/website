# WEBSITE

Dunque.
Sapevo che prima o poi questo momento sarebbe dovuto arrivare.
Con un po' di voglia mi sono messo a scrivere quello che spero si evolverà in un sito web vero e proprio che potrò usare per qualcosa in più che farlo vedere a chiunque mi capiti a tiro per scovarmi bug.

Per ora ho un paio di obiettivi, avendo creato già 3/4 di ciò che volevo avere nel sito :
- Evitare il reflow del testo quando la pagina viene ingrandita troppo o visualizzata su finestre piccole o con aspect ratio strani (provate a prendere la pagina e ad aprirla in un browser largo metà schermo..)
- Creare un po' di design reattivo e qualcosina in JS per avere tutto animato e figo e bello e *profeshonal*
- Aggiungere qualcosa a quel \[\[qualcosa]] (si accettano suggerimenti)

Il tutto è con licenza MIT, potete farne che cazzo vi pare.

Sono ben accette segnalazioni di eventuali bug.